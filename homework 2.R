train<-read.csv("train.csv",sep=",")
str(train)
test<-read.csv("test.csv",sep=",")
str(test)

#chacking NA
any(is.na(train))
any(is.na(test))
install.packages('caTools')
library('caTools')

#taking numeric columns from train tabel
cols.numeric<-sapply(train,is.numeric)
cols.numeric
train.numeric<-train[,cols.numeric]

#1
cor.data<-cor(train.numeric)
install.packages("corrplot")
library("corrplot")
print(corrplot(cor.data,method='color'))

#2
mod <- lm(season ~ ., data=train)
summary(mod)
