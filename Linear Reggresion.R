#------Package List------

#------Chart------
install.packages("ggplot2")
library(ggplot2)

#------Linear Model------
install.packages('caTools')
library(caTools)

install.packages('dplyr')
library(dplyr)

#------Missing values------
install.packages('Amelia')
library(Amelia)

#------Correlations------ 
install.packages("corrplot")
library(corrplot)

#------Kmeans algorithm------
install.packages('cluster')
library(cluster)

#------SQL------
install.packages('RSQLite')
library(RSQLite)

install.packages('sqldf')
library(sqldf)

#------Text mining / Naive base------
install.packages('tm')
library(tm)

install.packages('e1071')
library(e1071)

install.packages('wordcloud')
library(wordcloud)

install.packages('SDMTools')
library(SDMTools)

#------Decision trees------
install.packages('rpart')
library(rpart)

install.packages('rpart.plot')
library(rpart.plot)

install.packages('ISLR')
library('ISLR')

#------Other Useful------
install.packages(c("caret","quanteda","irlba", "randomForetst"))








bike <- read.csv("bikes.csv")
## Show strtucture
str(bike)
## Show all data
View(bike)
## change datetime to charType
bike$datetime <- as.character(bike$datetime)
## Then we split date and time by blank sapply is like loop. 
bike$date <- sapply(strsplit(as.character(bike$datetime),' '), "[", 1)
bike$hour <- sapply(strsplit(as.character(bike$datetime),' '), "[", 2)
## We cast the date to date 
bike$date <- as.Date(bike$date)
## We take the number from the bike 
bike$hour <- sapply(strsplit(as.character(bike$hour),':'), "[", 1)
#We cast the hour to a number 
bike$hour <- as.numeric(bike$hour)
# Making season a factor 
# 1 = spring, 2 = summer, 3 = fall, 4 = winter 
bike$season <- factor(bike$season, levels = c(1,2,3,4), labels = c('spring', 'summer', 'fall','winter' ))
## Chacking that this is indeed a season 
levels(bike$season)

## How temterature affects the count? 
## ?????? ???????????????????? ???????? ???????? ???????????? ???????? ????????
ggplot(bike, aes(temp,count)) + geom_point(alpha = 0.3, aes(color = temp))
ggplot(bike, aes(temp,count)) + geom_point(alpha = 0.3, aes(color = temp))+ theme_bw()

## Check the correlation between temp and count.
## the correlation is 0.4 which means not so high.
cor(bike[,c('temp', 'count')])

## What regression line look like
plot(bike$temp,bike$count)
abline(lm(bike$count ~ bike$temp))

## How date affects the count? 
## We can see that in July the color is red which means high temperature and high count rate
pl <- ggplot(bike, aes(date,count)) + geom_point(alpha = 0.5, aes(color = temp))
pl + scale_color_continuous(low = 'blue', high = 'red') +theme_bw()

## How season affcets the count 
ggplot(bike, aes(season,count)) + geom_boxplot(aes(color = season)) + theme_bw()

## On weekdays 
## how hours affects count in working day
## when the temperature high and hours between 15:00 - 20:00 count rate is high
pl <- ggplot(bike[bike$workingday != 1,], aes(hour,count))  
pl <- pl + geom_point(position = position_jitter(w= 0.5, h =0)  ,aes(color = temp))
pl <- pl + scale_colour_gradient(low = "blue", high = "red")
pl <- pl + theme_bw()

# On sunday
pl <- ggplot(bike[bike$workingday == 1,], aes(hour,count))  
pl <- pl + geom_point(position = position_jitter(w= 0.5, h =0)  ,aes(color = temp))
pl <- pl + scale_colour_gradient(low = "blue", high = "red")
pl <- pl + theme_bw()

## calc the 70% point
dim(bike)
## 10886 * 0.7 = 7620
bike$date[7620]
## 2012-05-16 is the date in 7 place

## here start Linear model
## Split into training set snd test set 
## 70% train set and 30% test
splitDate <- as.Date("2012-05-16")
View(bike)
dateFilter <- bike$date <= splitDate 

## split
bike.train <- bike[dateFilter,]
bike.test <-  bike[!dateFilter,]

## count rows
dim(bike.train)
dim(bike.test)

# we must remove datetime otherwiswe we'll get an error 
# lm treats datetime as a factor and throws an error message 
# becuase it finds new values in the test set 

bike.train$datetime <-NULL
bike.test$datetime <-NULL


## lm run only on training set!!!!!!

model <- lm(count ~ . -atemp -casual -registered -date, bike.train)

## the number of * indicates the importants of the subjects
summary(model)

predicted.train <- predict(model,bike.train)
predicted.test <- predict(model,bike.test)

## mean square error
MSE.train <- mean((bike.train$count - predicted.train)**2) ## 14390
MSE.test <- mean((bike.test$count - predicted.test)**2) ## 40794

## the numbers are not close which means overfitting

## Root of diviation
RMSE.train <- MSE.train**0.5
RMSE.test <- MSE.test**0.5

