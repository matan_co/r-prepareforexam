## Homework 1
##-> assigned air quality to x
x <- airquality
x
##-> 1. sum all "NA" in the Vector
sum(is.na(x))
##-> 2. if i would like to remove all NA from vector X
x.clean <- na.omit(x)
##-> 3. create avg for each column
avg_vector <- c(mean(x.clean$Ozone),mean(x.clean$Solar.R),mean(x.clean$Wind),mean(x.clean$Temp))
avg_vector
##-> 4. get the bigger rows of avg
first  <- subset(x.clean,Ozone>mean(x.clean$Ozone))
second <- subset(first,Solar.R>mean(x.clean$Solar.R))  
third  <- subset(second,Wind>mean(x.clean$Wind)) 
forth  <- subset(third,Temp>mean(x.clean$Temp)) 
forth
##-> 5. sort the dataset by
before.filter <- x.clean[order(x$Solar.R),]
before.filter
##-> 6. add column ratio
ratio <- x.clean$Ozone/x.clean$Solar.R
q2 <- cbind(x.clean,ratio)
q2
## way with apply
new <- subset(x.clean, select=c("Ozone", "Solar.R"))
ratios <- subset(apply(new, 2, function(ratio) new$Ozone / new$Solar.R), select = c("Ozone"))
colnames(ratios)[colnames(ratios)=="Ozone"] <- "Ratio"
ratios
##-> 7. 

par(mfrow = c(6,1))
plot(q2$Ozone, q2$Solar.R, main = "Ozone VS Solar.R")
plot(q2$Ozone, q2$Wind, main = "Ozone VS Wind")
plot(q2$Ozone, q2$Temp, main = "Ozone VS Temp")
plot(q2$Solar.R, q2$Wind, main = "Solar.R VS Wind")
plot(q2$Solar.R, q2$Temp, main = "Solar.R VS Temp")
plot(q2$Wind, q2$Temp, main = "Wind VS Temp")

